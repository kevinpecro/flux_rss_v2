from peewee import *
from flask_login import UserMixin

database = SqliteDatabase("users.sqlite3")


class BaseModel(Model):

    class Meta:
        database = database


class Users(BaseModel, UserMixin):
    username = CharField()
    password = CharField()

class Flux(BaseModel):
    flux = CharField()
    id_users = ForeignKeyField(Users, backref="flux")
    name = CharField()


def create_tables():
    with database:
        database.create_tables([Users])

def create_tables2():
    with database:
        database.create_tables([Flux])


def drop_tables():
    with database:
        database.drop_tables([Users])

def drop_tables2():
    with database:
        database.drop_tables([Flux])


