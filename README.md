# Projet Python Flux_rss




## Objectif ?
Le site est réalisé de la manière suivante :
À l'ouverture du site, l'index montre un flux rss de base qui est récupérer sur le site Le monde.
L'utilisateur peut alors créer un compte ou se connecter en utilisant les boutons dans l'en-tête du site.
une fois connecter, l'utilisateur peut accéder à son compte sur lequel il consulte les flux qu'il a enregistrer, il peut en ajouter, supprimer.
S'il clic sur un flux il pourra le visualiser sur une autre page et cliquer sur un article pour le consulter sur le site de l'éditeur.


## Pour lancer le projet:
Il faut executer les commandes suivantes :
_ pipenv shell
_ pipenv install
_ flask run

Pour tester, il existe un compte:
id : test
password : test
Vous pouvez également créer le votre.

## Auteur
> [Kevin Pecro]
> [Estelle Castetnau]
> [Steven Rouselle]